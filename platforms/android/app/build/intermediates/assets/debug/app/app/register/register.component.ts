import { Component } from "@angular/core";
import { HttpService } from '../services/http.service';
import { Location } from "@angular/common";
import * as ApplicationSettings from "application-settings";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Page } from "ui/page";

@Component({
    selector: "ns-register",
    moduleId: module.id,
    templateUrl: "register.component.html",
})
export class RegisterComponent {
    public input: any;
    processing: boolean = false;

    public constructor(private service: HttpService, private location: Location, page: Page) {
        page.actionBarHidden = true;
        this.input = {
            UserID: 0,
            Name: "",
            Username: "",
            Password: "",
            Role: "Worker"
        }
    }

    public register() {
        this.processing = true;
        this.service.post('/users', this.input)
            .subscribe(
                resp => {
                    if (resp['meta'].success) {
                        dialogs.alert({
                            title: "Success !",
                            message: "Save data success !",
                            okButtonText: "Ok"
                        }).then(() => {
                            this.location.back();
                        })
                    } else {
                        dialogs.alert({
                            title: "Failed !",
                            message: "Save data failed !",
                            okButtonText: "Ok"
                        }).then(() => {
                            console.log("Dialog closed!");
                        })
                    }
                    console.log(resp);
                    this.processing = false;
                },
                err => { },
                () => { }
            );
    }

    public goBack() {
        this.location.back();
    }

}