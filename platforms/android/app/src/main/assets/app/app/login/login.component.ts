import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpService } from '../services/http.service';
import * as ApplicationSettings from "application-settings";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Page } from "ui/page";

@Component({
    selector: "ns-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
})
export class LoginComponent implements OnInit {
    public input: any;
    processing: boolean = false;

    constructor(private service: HttpService, private router: RouterExtensions, page: Page) {
        page.actionBarHidden = true;
        this.input = {
            username: "",
            password: ""
        }
    }

    ngOnInit(): void {
        if (ApplicationSettings.getBoolean("authenticated", false)) {
            this.router.navigate(["/login"], { clearHistory: true });
        }
    }

    public login() {
        this.processing = true;
        this.service.post('/authenticate', this.input, 'Basic')
            .subscribe(
                res => {
                    if (res['data']) {
                        ApplicationSettings.setBoolean("authenticated", true);
                        ApplicationSettings.setString("auth-token", res['data']['token']);
                        ApplicationSettings.setNumber("UserID", res['data']['users']['UserID']);
                        
                        this.router.navigate(["/home"], { clearHistory: true });
                    } else {
                        dialogs.alert({
                            title: "Failed !",
                            message: "Username or password invalid !",
                            okButtonText: "Ok"
                        }).then(() => {
                            console.log("Dialog closed!");
                        })
                    }
                    this.processing = false;
                },
                err => { },
                () => { }
            )
    }
}