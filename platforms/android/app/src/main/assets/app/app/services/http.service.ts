import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { config } from '../config/app.config';
import { throwError, Observable } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';
import * as base64 from "base-64";
import * as utf8 from "utf8";
import * as ApplicationSettings from "application-settings";

@Injectable()
export class HttpService {
    base = config.base;

    constructor(private http: HttpClient) { }

    public get(url) {
        let res = this.http.get(this.base + url, this.getOption())
            .pipe(
                retry(config.retryTime),
                catchError(this.handleError)
            );
        return res;
    }

    public post(url, data: any, auth=null) {
        let option: any;
        option = (auth == 'Basic') ? this.getOption2(data) : this.getOption();

        let res = this.http.post(this.base + url, JSON.stringify(data), option)
            .pipe(
                retry(config.retryTime),
                catchError(this.handleError)
            );
        return res;
    }

    public put(url, data: any) {
        let res = this.http.put(this.base + url, JSON.stringify(data), this.getOption())
            .pipe(
                retry(config.retryTime),
                catchError(this.handleError)
            );
        return res;
    }

    public delete(url) {
        let res = this.http.delete(this.base + url, this.getOption())
            .pipe(
                retry(config.retryTime),
                catchError(this.handleError)
            );
        return res;
    }

    private getOption() {
        let token = ApplicationSettings.getString("auth-token") || "";
        let headers = new HttpHeaders({
            "Content-type": "application/json",
            "Authorization": "Bearer " + token
        });
        return { headers: headers };
    }

    private getOption2(data) {
        let str = data.username + ':' + data.password;
        let bytes = utf8.encode(str);
        let token = base64.encode(bytes);
        let headers = new HttpHeaders({
            "Content-type": "application/x-www-form-urlencoded",
            "Authorization": "Basic " + token
        });
        return { headers: headers };
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };
}