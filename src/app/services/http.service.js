"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var app_config_1 = require("../config/app.config");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var base64 = require("base-64");
var utf8 = require("utf8");
var ApplicationSettings = require("application-settings");
var HttpService = /** @class */ (function () {
    function HttpService(http) {
        this.http = http;
        this.base = app_config_1.config.base;
    }
    HttpService.prototype.get = function (url) {
        var res = this.http.get(this.base + url, this.getOption())
            .pipe(operators_1.retry(app_config_1.config.retryTime), operators_1.catchError(this.handleError));
        return res;
    };
    HttpService.prototype.post = function (url, data, auth) {
        if (auth === void 0) { auth = null; }
        var option;
        option = (auth == 'Basic') ? this.getOption2(data) : this.getOption();
        var res = this.http.post(this.base + url, JSON.stringify(data), option)
            .pipe(operators_1.retry(app_config_1.config.retryTime), operators_1.catchError(this.handleError));
        return res;
    };
    HttpService.prototype.put = function (url, data) {
        var res = this.http.put(this.base + url, JSON.stringify(data), this.getOption())
            .pipe(operators_1.retry(app_config_1.config.retryTime), operators_1.catchError(this.handleError));
        return res;
    };
    HttpService.prototype.delete = function (url) {
        var res = this.http.delete(this.base + url, this.getOption())
            .pipe(operators_1.retry(app_config_1.config.retryTime), operators_1.catchError(this.handleError));
        return res;
    };
    HttpService.prototype.getOption = function () {
        var token = ApplicationSettings.getString("auth-token") || "";
        var headers = new http_1.HttpHeaders({
            "Content-type": "application/json",
            "Authorization": "Bearer " + token
        });
        return { headers: headers };
    };
    HttpService.prototype.getOption2 = function (data) {
        var str = data.username + ':' + data.password;
        var bytes = utf8.encode(str);
        var token = base64.encode(bytes);
        var headers = new http_1.HttpHeaders({
            "Content-type": "application/x-www-form-urlencoded",
            "Authorization": "Basic " + token
        });
        return { headers: headers };
    };
    HttpService.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error("Backend returned code " + error.status + ", " +
                ("body was: " + error.error));
        }
        // return an observable with a user-facing error message
        return rxjs_1.throwError('Something bad happened; please try again later.');
    };
    ;
    HttpService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], HttpService);
    return HttpService;
}());
exports.HttpService = HttpService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHR0cC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaHR0cC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLDZDQUFrRjtBQUNsRixtREFBOEM7QUFDOUMsNkJBQThDO0FBQzlDLDRDQUF3RDtBQUN4RCxnQ0FBa0M7QUFDbEMsMkJBQTZCO0FBQzdCLDBEQUE0RDtBQUc1RDtJQUdJLHFCQUFvQixJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBRnBDLFNBQUksR0FBRyxtQkFBTSxDQUFDLElBQUksQ0FBQztJQUVxQixDQUFDO0lBRWxDLHlCQUFHLEdBQVYsVUFBVyxHQUFHO1FBQ1YsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2FBQ3JELElBQUksQ0FDRCxpQkFBSyxDQUFDLG1CQUFNLENBQUMsU0FBUyxDQUFDLEVBQ3ZCLHNCQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUMvQixDQUFDO1FBQ04sTUFBTSxDQUFDLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFTSwwQkFBSSxHQUFYLFVBQVksR0FBRyxFQUFFLElBQVMsRUFBRSxJQUFTO1FBQVQscUJBQUEsRUFBQSxXQUFTO1FBQ2pDLElBQUksTUFBVyxDQUFDO1FBQ2hCLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBRXRFLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsTUFBTSxDQUFDO2FBQ2xFLElBQUksQ0FDRCxpQkFBSyxDQUFDLG1CQUFNLENBQUMsU0FBUyxDQUFDLEVBQ3ZCLHNCQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUMvQixDQUFDO1FBQ04sTUFBTSxDQUFDLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFTSx5QkFBRyxHQUFWLFVBQVcsR0FBRyxFQUFFLElBQVM7UUFDckIsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7YUFDM0UsSUFBSSxDQUNELGlCQUFLLENBQUMsbUJBQU0sQ0FBQyxTQUFTLENBQUMsRUFDdkIsc0JBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQy9CLENBQUM7UUFDTixNQUFNLENBQUMsR0FBRyxDQUFDO0lBQ2YsQ0FBQztJQUVNLDRCQUFNLEdBQWIsVUFBYyxHQUFHO1FBQ2IsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2FBQ3hELElBQUksQ0FDRCxpQkFBSyxDQUFDLG1CQUFNLENBQUMsU0FBUyxDQUFDLEVBQ3ZCLHNCQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUMvQixDQUFDO1FBQ04sTUFBTSxDQUFDLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFTywrQkFBUyxHQUFqQjtRQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDOUQsSUFBSSxPQUFPLEdBQUcsSUFBSSxrQkFBVyxDQUFDO1lBQzFCLGNBQWMsRUFBRSxrQkFBa0I7WUFDbEMsZUFBZSxFQUFFLFNBQVMsR0FBRyxLQUFLO1NBQ3JDLENBQUMsQ0FBQztRQUNILE1BQU0sQ0FBQyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRU8sZ0NBQVUsR0FBbEIsVUFBbUIsSUFBSTtRQUNuQixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQzlDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDN0IsSUFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqQyxJQUFJLE9BQU8sR0FBRyxJQUFJLGtCQUFXLENBQUM7WUFDMUIsY0FBYyxFQUFFLG1DQUFtQztZQUNuRCxlQUFlLEVBQUUsUUFBUSxHQUFHLEtBQUs7U0FDcEMsQ0FBQyxDQUFDO1FBQ0gsTUFBTSxDQUFDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFTyxpQ0FBVyxHQUFuQixVQUFvQixLQUF3QjtRQUN4QyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxZQUFZLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDcEMsa0VBQWtFO1lBQ2xFLE9BQU8sQ0FBQyxLQUFLLENBQUMsb0JBQW9CLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM3RCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixzREFBc0Q7WUFDdEQsNkRBQTZEO1lBQzdELE9BQU8sQ0FBQyxLQUFLLENBQ1QsMkJBQXlCLEtBQUssQ0FBQyxNQUFNLE9BQUk7aUJBQ3pDLGVBQWEsS0FBSyxDQUFDLEtBQU8sQ0FBQSxDQUFDLENBQUM7UUFDcEMsQ0FBQztRQUNELHdEQUF3RDtRQUN4RCxNQUFNLENBQUMsaUJBQVUsQ0FDYixpREFBaUQsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFBQSxDQUFDO0lBOUVPLFdBQVc7UUFEdkIsaUJBQVUsRUFBRTt5Q0FJaUIsaUJBQVU7T0FIM0IsV0FBVyxDQStFdkI7SUFBRCxrQkFBQztDQUFBLEFBL0VELElBK0VDO0FBL0VZLGtDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycywgSHR0cEVycm9yUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IGNvbmZpZyB9IGZyb20gJy4uL2NvbmZpZy9hcHAuY29uZmlnJztcclxuaW1wb3J0IHsgdGhyb3dFcnJvciwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyByZXRyeSwgY2F0Y2hFcnJvciwgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgKiBhcyBiYXNlNjQgZnJvbSBcImJhc2UtNjRcIjtcclxuaW1wb3J0ICogYXMgdXRmOCBmcm9tIFwidXRmOFwiO1xyXG5pbXBvcnQgKiBhcyBBcHBsaWNhdGlvblNldHRpbmdzIGZyb20gXCJhcHBsaWNhdGlvbi1zZXR0aW5nc1wiO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgSHR0cFNlcnZpY2Uge1xyXG4gICAgYmFzZSA9IGNvbmZpZy5iYXNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkgeyB9XHJcblxyXG4gICAgcHVibGljIGdldCh1cmwpIHtcclxuICAgICAgICBsZXQgcmVzID0gdGhpcy5odHRwLmdldCh0aGlzLmJhc2UgKyB1cmwsIHRoaXMuZ2V0T3B0aW9uKCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgcmV0cnkoY29uZmlnLnJldHJ5VGltZSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKHRoaXMuaGFuZGxlRXJyb3IpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgcmV0dXJuIHJlcztcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgcG9zdCh1cmwsIGRhdGE6IGFueSwgYXV0aD1udWxsKSB7XHJcbiAgICAgICAgbGV0IG9wdGlvbjogYW55O1xyXG4gICAgICAgIG9wdGlvbiA9IChhdXRoID09ICdCYXNpYycpID8gdGhpcy5nZXRPcHRpb24yKGRhdGEpIDogdGhpcy5nZXRPcHRpb24oKTtcclxuXHJcbiAgICAgICAgbGV0IHJlcyA9IHRoaXMuaHR0cC5wb3N0KHRoaXMuYmFzZSArIHVybCwgSlNPTi5zdHJpbmdpZnkoZGF0YSksIG9wdGlvbilcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICByZXRyeShjb25maWcucmV0cnlUaW1lKSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IodGhpcy5oYW5kbGVFcnJvcilcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICByZXR1cm4gcmVzO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBwdXQodXJsLCBkYXRhOiBhbnkpIHtcclxuICAgICAgICBsZXQgcmVzID0gdGhpcy5odHRwLnB1dCh0aGlzLmJhc2UgKyB1cmwsIEpTT04uc3RyaW5naWZ5KGRhdGEpLCB0aGlzLmdldE9wdGlvbigpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIHJldHJ5KGNvbmZpZy5yZXRyeVRpbWUpLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcih0aGlzLmhhbmRsZUVycm9yKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIHJldHVybiByZXM7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGRlbGV0ZSh1cmwpIHtcclxuICAgICAgICBsZXQgcmVzID0gdGhpcy5odHRwLmRlbGV0ZSh0aGlzLmJhc2UgKyB1cmwsIHRoaXMuZ2V0T3B0aW9uKCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgcmV0cnkoY29uZmlnLnJldHJ5VGltZSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKHRoaXMuaGFuZGxlRXJyb3IpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgcmV0dXJuIHJlcztcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldE9wdGlvbigpIHtcclxuICAgICAgICBsZXQgdG9rZW4gPSBBcHBsaWNhdGlvblNldHRpbmdzLmdldFN0cmluZyhcImF1dGgtdG9rZW5cIikgfHwgXCJcIjtcclxuICAgICAgICBsZXQgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAgIFwiQ29udGVudC10eXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxyXG4gICAgICAgICAgICBcIkF1dGhvcml6YXRpb25cIjogXCJCZWFyZXIgXCIgKyB0b2tlblxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiB7IGhlYWRlcnM6IGhlYWRlcnMgfTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldE9wdGlvbjIoZGF0YSkge1xyXG4gICAgICAgIGxldCBzdHIgPSBkYXRhLnVzZXJuYW1lICsgJzonICsgZGF0YS5wYXNzd29yZDtcclxuICAgICAgICBsZXQgYnl0ZXMgPSB1dGY4LmVuY29kZShzdHIpO1xyXG4gICAgICAgIGxldCB0b2tlbiA9IGJhc2U2NC5lbmNvZGUoYnl0ZXMpO1xyXG4gICAgICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgXCJDb250ZW50LXR5cGVcIjogXCJhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWRcIixcclxuICAgICAgICAgICAgXCJBdXRob3JpemF0aW9uXCI6IFwiQmFzaWMgXCIgKyB0b2tlblxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiB7IGhlYWRlcnM6IGhlYWRlcnMgfTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGhhbmRsZUVycm9yKGVycm9yOiBIdHRwRXJyb3JSZXNwb25zZSkge1xyXG4gICAgICAgIGlmIChlcnJvci5lcnJvciBpbnN0YW5jZW9mIEVycm9yRXZlbnQpIHtcclxuICAgICAgICAgICAgLy8gQSBjbGllbnQtc2lkZSBvciBuZXR3b3JrIGVycm9yIG9jY3VycmVkLiBIYW5kbGUgaXQgYWNjb3JkaW5nbHkuXHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0FuIGVycm9yIG9jY3VycmVkOicsIGVycm9yLmVycm9yLm1lc3NhZ2UpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIFRoZSBiYWNrZW5kIHJldHVybmVkIGFuIHVuc3VjY2Vzc2Z1bCByZXNwb25zZSBjb2RlLlxyXG4gICAgICAgICAgICAvLyBUaGUgcmVzcG9uc2UgYm9keSBtYXkgY29udGFpbiBjbHVlcyBhcyB0byB3aGF0IHdlbnQgd3JvbmcsXHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXHJcbiAgICAgICAgICAgICAgICBgQmFja2VuZCByZXR1cm5lZCBjb2RlICR7ZXJyb3Iuc3RhdHVzfSwgYCArXHJcbiAgICAgICAgICAgICAgICBgYm9keSB3YXM6ICR7ZXJyb3IuZXJyb3J9YCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIHJldHVybiBhbiBvYnNlcnZhYmxlIHdpdGggYSB1c2VyLWZhY2luZyBlcnJvciBtZXNzYWdlXHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoXHJcbiAgICAgICAgICAgICdTb21ldGhpbmcgYmFkIGhhcHBlbmVkOyBwbGVhc2UgdHJ5IGFnYWluIGxhdGVyLicpO1xyXG4gICAgfTtcclxufSJdfQ==