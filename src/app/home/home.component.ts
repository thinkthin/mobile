import { Component, OnInit, AfterViewInit } from "@angular/core";
import { HttpService } from '../services/http.service';
import { RouterExtensions } from "nativescript-angular/router";
import * as ApplicationSettings from "application-settings";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as geolocation from "nativescript-geolocation";
import { Accuracy } from 'tns-core-modules/ui/enums';

@Component({
    selector: "ns-home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
})

export class HomeComponent implements OnInit {
    processing: boolean = false;
    userID: number = ApplicationSettings.getNumber("UserID");
    watchId: any;
    trackName: string;
    loc = {
        lat: null,
        lng: null
    };
    trackingOff: boolean = true;
    x: number = 0;

    // tracks
    tracks = {
        TrackID: 0,
        UserID: 0,
        TrackName: ''
    };

    // trackhistories
    trackhistories = {
        TrackHistoryID: 0,
        TrackID: 0,
        Latitude: 0.0,
        Longitude: 0.0
    };

    // trackstatuses
    trackstatuses = {
        TrackStatusID: 0,
        TrackID: 0,
        TrackStatus: ''
    };


    constructor(private service: HttpService, private router: RouterExtensions) { }

    ngOnInit(): void { }

    request() {
        console.log('enableLocationRequest()');
        geolocation.enableLocationRequest().then(() => {
            console.log('location enabled!');
            this.watch();
        }, e => {
            console.log('Failed to enable', e);
        });
    }

    watch() {
        this.watchId = geolocation.watchLocation(position => {
            // set lat long
            this.loc.lat = position.latitude;
            this.loc.lng = position.longitude;

            // trackhistories
            this.trackhistories.Latitude = this.loc.lat;
            this.trackhistories.Longitude = this.loc.lng;
            this.service.post('/trackhistories', this.trackhistories)
                .subscribe(
                    resp => {
                        if (resp['meta'].success) {
                            console.log('Insert trackshistories success !');
                        } else {
                            console.log('Insert trackshistories failed !');
                            console.log(this.trackhistories);
                            console.log(resp);
                        }
                    },
                    err => { },
                    () => { }
                );

            // debug
            this.x = this.x + 1;
            console.log(this.x + ' => ' + position.latitude);
            console.log(this.x + ' => ' + position.longitude);
        }, e => {
            console.log('Failed to get location');
        }, {
                desiredAccuracy: Accuracy.high,
                minimumUpdateTime: 1000
            });
    }

    startTracking() {
        this.processing = true;
        this.trackingOff = false;
        geolocation.isEnabled().then(enabled => {
            if (enabled) {
                this.tracks.TrackName = this.trackName;
                this.tracks.UserID = this.userID;
                this.service.post('/tracks', this.tracks)
                    .subscribe(
                        resp => {
                            if (resp['meta'].success) {
                                console.log('Insert tracks success !');
                                this.trackhistories.TrackID = resp['data'].TrackID;
                                this.trackstatuses.TrackID = resp['data'].TrackID;
                                this.trackstatuses.TrackStatus = 'Progress';
                                this.trackStatus();
                                this.watch();
                            } else {
                                console.log('Insert tracks failed !');
                            }
                            this.processing = false;
                        },
                        err => { },
                        () => { }
                    );
            } else {
                this.request();
            }
        }, e => {
            console.log('isEnabled error', e);
            this.request();
        });
    }

    endTracking() {
        this.processing = true;
        this.trackingOff = true;
        let msg: string = 'failed';
        if (this.watchId) {
            geolocation.clearWatch(this.watchId);
            msg = 'success';
            this.trackName = '';
            this.loc.lat = '';
            this.loc.lng = '';
            this.x = 0;
            this.trackstatuses.TrackStatus = 'End';
            this.trackStatus();
            this.processing = false;
        }

        dialogs.alert({
            title: msg,
            message: msg,
            okButtonText: "Ok"
        }).then(() => {
            console.log(msg);
        });
    }

    trackStatus() {
        this.service.post('/trackstatuses', this.trackstatuses)
            .subscribe(
                resp => {
                    if (resp['meta'].success) {
                        console.log('Insert trackstatuses success !');
                    } else {
                        console.log('Insert trackstatuses failed !');
                    }
                },
                err => { },
                () => { }
            );
    }

    logout() {
        ApplicationSettings.remove("authenticated");
        this.router.navigate(["/login"], { clearHistory: true });
    }

}